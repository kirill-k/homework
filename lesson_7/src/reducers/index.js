import { combineReducers } from 'redux';
import posts from './posts';
import postItem from './posts/postItem';
import userPosts from './posts/userPosts';
import comments from './posts/comments';

const reducer = combineReducers({
    posts,
    postItem,
    comments,
    userPosts
});

export default reducer;
