import { GET_DATA_REQUESTED, GET_POST_ITEM_SUCCESS, POST_ERROR } from '../../constants';

const postsInitialState = {
    loading: false,
    loaded: false,
    post: {},
    errors: []
};

const postItem = ( state = postsInitialState, action) => {
    switch( action.type ){

        case GET_DATA_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case POST_ERROR:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.data
            };

        case GET_POST_ITEM_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                post: action.data
            };

        default:
            return state;
    }
};

export default postItem;
