import { GET_DATA_REQUESTED, POST_SUCCESS, POST_ERROR } from '../../constants';

const postsInitialState = {
    loading: false,
    loaded: false,
    data: [],
    errors: []
};

const posts = ( state = postsInitialState, action) => {
    switch( action.type ){

        case GET_DATA_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case POST_ERROR:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.data
            };

        case POST_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                data: action.data
            };

        default:
            return state;
    }
};

export default posts;
