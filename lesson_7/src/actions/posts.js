/* eslint-disable */
import React from 'react'
import axios from 'axios';
import * as actionTypes from '../constants';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const getDataDone = data => {
    return {
        type: actionTypes.POST_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.POST_ERROR
    };
};

export const getPostsData = () => dispatch => {
    const url = 'https://jsonplaceholder.typicode.com/posts';

    dispatch(getDataRequested());

    axios.get(url)
        .then(res => {
            const data = res.data;
            dispatch(getDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
};