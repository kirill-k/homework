/* eslint-disable */
import React from 'react'
import axios from 'axios';
import * as actionTypes from '../constants';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const getUsersDataDone = data => {
    return {
        type: actionTypes.GET_USER_POSTS_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.POST_ERROR
    };
};

export const getUserPostsData = (id) => dispatch => {
    const url = `https://jsonplaceholder.typicode.com/posts?userId=${id}`;

    dispatch(getDataRequested());

    axios.get(url)
        .then(res => {
            const data = res.data;
            dispatch(getUsersDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
};