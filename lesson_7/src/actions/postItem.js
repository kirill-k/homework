/* eslint-disable */
import React from 'react'
import axios from 'axios';
import * as actionTypes from '../constants';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const getPostDataDone = data => {
    return {
        type: actionTypes.GET_POST_ITEM_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.POST_ERROR
    };
};

export const getPostItemData = (id) => dispatch => {
    const url = `https://jsonplaceholder.typicode.com/posts/${id}`;

    dispatch(getDataRequested());

    axios.get(url)
        .then(res => {
            const data = res.data;
            dispatch(getPostDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
}