/* eslint-disable */
import React from 'react'
import axios from 'axios';
import * as actionTypes from '../constants';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const getDataCommentsDone = data => {
    return {
        type: actionTypes.GET_POST_COMMENTS_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.POST_ERROR
    };
};

export const getPostCommentsData = (id) => dispatch => {
    const url = `https://jsonplaceholder.typicode.com/posts/${id}/comments`;

    dispatch(getDataRequested());

    axios.get(url)
        .then(res => {
            const data = res.data;
            dispatch(getDataCommentsDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
};