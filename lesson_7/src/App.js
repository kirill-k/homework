import React, { Component } from 'react';
import {Routes} from './routes'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from "react-redux";
import store from './redux/store';


class App extends Component {
    render() {
        return (
            <div>
                <Provider store={store}>
                    <BrowserRouter>
                        <div>
                            <div className="menu">
                            </div>
                            <Switch>
                                {
                                    Routes.map((route, index) => {
                                        return <Route key={index} {...route} />
                                    })
                                }
                            </Switch>
                        </div>
                    </BrowserRouter>
                </Provider>
            </div>
        );
    }
}

export default App;
