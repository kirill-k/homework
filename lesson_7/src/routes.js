/* eslint-disable */
import React from 'react';
import Main from './components/index'
import PostBody from './components/postBody'
import AuthorPosts from './components/authorPosts'
import PostComments from './components/comments'

const notFound = () => (<div className="mainLayout">404</div>)

export const Routes = [
    {
        path: '/',
        exact: true,
        component: Main,
    },
    {
        path: '/posts/:id',
        exact: true,
        component: PostBody
    },
    {
        path: '/posts/:id/comments',
        component: PostComments
    },
    {
        path: '/user/:id',
        component: AuthorPosts
    },
    {
        exact: true,
        component: notFound
    }
];