/* eslint-disable */
import React, { Component } from 'react'
import Loading from './common/loader'
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as postActions from '../actions/userPosts';

import '../index.css'

class AuthorPosts extends Component{

    componentDidMount(){
        const id = this.props.match.params.id;
        this.props.getUserPostsData(id);
    }

    render = () => {
        const {
            userPosts: {
                posts,
                loading
            }
        } = this.props;

        if( loading === true){
            return <Loading />
        } else {
            return (
                <div className="postBlock">
                    <Helmet
                        htmlAttributes={{"lang": "en", "amp": undefined}}
                        title="Author Posts"
                        meta={[
                            {"name": "description", "content": "home-task #7"},
                        ]}
                    />
                    {
                        posts.map(post => {
                            return (
                                <div key={post.id}>
                                    <p>{post.body}</p>
                                    <h2>{post.title}</h2>
                                </div>
                            )
                        })
                    }
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        userPosts: state.userPosts
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserPostsData: data => dispatch(postActions.getUserPostsData(data)),
    };
};

AuthorPosts.propTypes = {
    userPosts: PropTypes.obj,
    getUserPostsData: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthorPosts);
