/* eslint-disable */
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import Loading from './common/loader'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import * as postActions from '../actions/postItem';

import '../index.css'

class PostBody extends Component{

    componentDidMount(){
        const id = this.props.match.params.id;
        this.props.getPostItemData(id)
    }

    render () {
        const { postItem, match } = this.props;

        if( postItem.loading === true){
            return(<Loading />)
        } else {
            return(
                <div className="postBlock">
                    <Helmet
                        htmlAttributes={{"lang": "en", "amp": undefined}}
                        title="Post Description"
                        meta={[
                            {"name": "description", "content": "home-task #7"},
                        ]}
                    />
                    <Link to={`/user/${postItem.post.userId}`}>{`Author: #${postItem.post.userId}`}</Link>
                    <h3>{postItem.post.title}</h3>
                    <p>{postItem.post.body}</p>
                    <Link to={`${match.url}/comments/`}>Comments</Link>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return { postItem: state.postItem };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getPostItemData: data => dispatch(postActions.getPostItemData(data)),
    };
};

PostBody.propTypes = {
    getPostItemData: PropTypes.func,
    postItem: PropTypes.obj,
    to: PropTypes.string
};

export default connect(mapStateToProps, mapDispatchToProps)(PostBody);
