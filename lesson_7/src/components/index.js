/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PostList from './postList'
import * as postActions from '../actions/posts';

import '../index.css'

class MainComponent extends Component {

    componentDidMount = () => {

        this.props.getPostsData();
    };


    render() {
        const { posts } = this.props;
        return (
            <div className="App">
               <PostList posts={posts.data} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state.posts
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getPostsData: data => dispatch(postActions.getPostsData(data))
    };
};

MainComponent.propTypes = {
    getPostsData: PropTypes.func,
    posts: PropTypes.obj
};

export default connect(mapStateToProps, mapDispatchToProps)(MainComponent);
