/* eslint-disable */
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import '../index.css'

class PostItem extends Component {
    render() {
        const postItem = this.props.data;

        return <Link to={`/posts/${postItem.id}`}>{postItem.title}</Link>
    }
}

PostItem.propTypes = {
    to: PropTypes.string
};

export default PostItem;