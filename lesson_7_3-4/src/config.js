export default {
    /* Apps config */
    supportedLangs: ['en', 'ru'],
    defaultLang: 'ru'
}