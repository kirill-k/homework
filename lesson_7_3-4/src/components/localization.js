import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import intlComponent from './intlContainer';
import Preview from './preview';
import config from '../config';

const SupportedLangs = config.supportedLangs.join('|');

const LocalizationComponent = () => {
    return(
        <Switch>
            <Route path={`/:locale(${SupportedLangs})`} component={intlComponent} exact={true} />
            <Route path={`/:locale(${SupportedLangs})/preview`} exact={true} component={Preview} />
            <Redirect to={`${config.defaultLang}`} />
        </Switch>
    )
};

export default LocalizationComponent;
