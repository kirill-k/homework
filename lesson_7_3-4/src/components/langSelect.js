import React from 'react';
import { NavLink } from 'react-router-dom';
import PathToRegexp, { compile } from 'path-to-regexp';

import config from '../config';
const SupportedLangs = config.supportedLangs.join('|');

const LangSelect = props => {
    const { location, match } = props;
    const ROUTE = `/:locale(${SupportedLangs})/:path*`;
    const routeComponents = PathToRegexp(ROUTE).exec(location.pathname);
    const definePath = compile(ROUTE);

    return(
        <div className="langSwitch">
            <div>
                {
                    config.supportedLangs.map( (lang, index) => {
                        if( lang !== match.params.locale){
                            return(
                                <NavLink
                                    style={{paddingRight: '20px', fontSize: '20px'}}
                                    key={index}
                                    to={definePath({ locale: lang, path: routeComponents[2] })}>
                                    {lang}
                                </NavLink>
                            )
                        } else {
                            return(
                                <span
                                    style={{paddingRight: '20px', fontSize: '20px', color: 'red'}}
                                    key={index}
                                >
                                    {lang}
                                </span>
                            )
                        }
                    })
                }
            </div>
        </div>
    )

};

export default LangSelect;
