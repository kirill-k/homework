/* eslint-disable */
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'
import {FormattedMessage} from 'react-intl';
import '../index.css'

class Preview extends Component {

    state = {
        data: JSON.parse(localStorage.getItem('state'))
    };


    render() {
        const { data } = this.state;
        console.log(this.props)
        const userData = data ? data.form.userData.values: [];

            return (
                <div>
                    <NavLink to="/">
                        <FormattedMessage
                            id="preview.return"
                            defaultMessage="Return"
                        />
                    </NavLink>
                    <h3>Data from local storage</h3>
                    <div>
                        <h2>Name</h2>
                        <p>{`First name: ${userData.firstName}`}</p>
                        <p>{`Last name: ${userData.lastName}`}</p>
                    </div>
                    <div>
                        <h2>Education</h2>
                        <p>{`City: ${userData.education.city}`}</p>
                        <p>{`Country: ${userData.education.country}`}</p>
                        <p>{`University: ${userData.education.university}`}</p>
                    </div>
                    <div>
                        <h2>Address</h2>
                        <p>{`Country: ${userData.address.city}`}</p>
                        <p>{`City: ${userData.education.country}`}</p>
                        <p>{`Street: ${userData.education.university}`}</p>
                    </div>
                </div>
            );
    }
}

export default Preview;
