import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Main from './index';
import LangSelect from './langSelect';
import Preview from './preview';

const Root = ({ match, location }) => {

    return(
        <div>
            <LangSelect location={location} match={match}/>
            <Switch>
                <Route path={`${match.url}/`} exact component={Main}/>
                <Route path={`${match.url}/preview`} component={Preview}/>
            </Switch>
        </div>
    )
};

export default Root;
