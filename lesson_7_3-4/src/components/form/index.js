/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, FormSection } from 'redux-form';
import { FormattedMessage } from 'react-intl';
import '../../index.css'

class Form extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
    };

    render() {

        return (
            <form style={{padding: '50px'}} name="data" onSubmit={this.handleSubmit}>
                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                    <div>
                        <h3>
                            <FormattedMessage id="user.detailsTitle"
                                              defaultMessage="User Details"
                            />
                        </h3>
                        <p>
                            <FormattedMessage id="user.firstName"
                                              defaultMessage="First Name"
                            />
                        </p>
                        <Field name="firstName" component="input" type="text"/>
                        <p>
                            <FormattedMessage id="user.lastName"
                                              defaultMessage="Last Name"
                            />
                        </p>
                        <Field name="lastName" component="input" type="text"/>
                    </div>
                    <div>
                        <h3>
                            <FormattedMessage id="user.educationTitle"
                                              defaultMessage="Education:"
                            />
                        </h3>
                        <FormSection name="education">
                            <p>
                                <FormattedMessage id="user.university"
                                                  defaultMessage="University:"
                                />
                            </p>
                            <Field name="university" component="input" type="text"/>
                            <p>
                                <FormattedMessage id="user.educCity"
                                                  defaultMessage="City:"
                                />
                            </p>
                            <Field name="city" component="input" type="text"/>
                            <p>
                                <FormattedMessage id="user.educCountry"
                                                  defaultMessage="University:"
                                />
                            </p>
                            <Field name="country" component="input" type="text"/>
                        </FormSection>
                    </div>
                    <div>
                        <h3>
                            <FormattedMessage id="user.address"
                                              defaultMessage="Address:"
                            />
                        </h3>
                        <FormSection name="address">
                            <p>
                                <FormattedMessage id="user.country"
                                                  defaultMessage="Country:"
                                />
                            </p>
                            <Field name="country" component="input" type="text"/>
                            <p>
                                <FormattedMessage id="user.city"
                                                  defaultMessage="City:"
                                />
                            </p>
                            <Field name="city" component="input" type="text"/>
                            <p>
                                <FormattedMessage id="user.street"
                                                  defaultMessage="Street:"
                                />
                            </p>
                            <Field name="street" component="input" type="text"/>
                        </FormSection>
                    </div>
                </div>
                <div>
                    <button type="submit">
                        <FormattedMessage id="user.buttonSend"
                                          defaultMessage="Send data"
                        />
                    </button>
                </div>
            </form>
        );
    }
}

reduxForm.propTypes = {
    name: PropTypes.string.isRequired,
    component: PropTypes.string
};


export default (reduxForm({
    form: 'userData',
    destroyOnUnmount: false
})(Form));
