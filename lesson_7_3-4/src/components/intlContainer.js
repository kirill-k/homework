import React from 'react';
import Root from './root';
import { Route } from 'react-router-dom';
import { addLocaleData, IntlProvider } from 'react-intl'
import en from 'react-intl/locale-data/en';
import ru from 'react-intl/locale-data/ru';

import translations from '../translations';


addLocaleData([...en, ...ru]);

const IntlComponent = ({match}) => {
    let locale = match.params.locale;

    return(
        <IntlProvider
            locale={locale}
            key={locale}
            messages={translations[locale]}
        >
            <Route component={Root} />
        </IntlProvider>
    );
};


export default IntlComponent;
