/* eslint-disable */
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types';
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';
import Form from './form'
import '../index.css'

class Main extends Component {

    handleSubmit = (values) => {
        console.log(values);
    };

    render() {
        const { match } = this.props;

        return (
            <div style={{padding: '30px'}}>
                <h1 className="App-title">
                    <FormattedMessage id="app.title"
                                      defaultMessage="Welcome to Kirill's App}"
                                      description="test"
                    />
                </h1>
                <h3 className="App-title">
                    <FormattedHTMLMessage id="app.intro"
                                      defaultMessage="To get started, edit src/App.js and save to reload."
                                      description="Text on main page"
                    />
                </h3>
                <NavLink to={`${match.url}/preview`}>
                    <FormattedHTMLMessage id="app.preview"
                                          defaultMessage="Preview"
                                          description="Text on main page"
                    />
                </NavLink>
                <Form onSubmit={this.handleSubmit} />
            </div>
        );
    }
}



Main.propTypes = {
    to: PropTypes.string
};

export default Main;
