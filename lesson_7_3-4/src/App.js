import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "react-redux";
import LocalizationComponent from './components/localization'
import store from './redux/store';

const supportsHistory = 'pushState' in window.history;
class App extends Component {

    render() {

        return (
            <div>
                <Provider store={store}>
                    <BrowserRouter forceRefrech={!supportsHistory}>
                        <LocalizationComponent />
                    </BrowserRouter>
                </Provider>
            </div>
        );
    }
}

export default App;
