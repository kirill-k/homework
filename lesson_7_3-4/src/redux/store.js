import { createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';
import { reducer as formReducer } from 'redux-form'
import { combineReducers } from 'redux';
import { loadState, saveState } from '../components/localStorage'

const reducers = {
    form: formReducer
};

const composeEnhancers = process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

const middleware = applyMiddleware(thunk);

const persistedState = loadState();

const reducer = combineReducers(reducers);

const store = createStore( reducer, persistedState, composeEnhancers(middleware) );

store.subscribe(() => {
    saveState(store.getState());
});

export default store;
