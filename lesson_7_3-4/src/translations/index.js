import Common from './common';
import config from '../config';

const TranslationsArray = [Common];
const ConnectedTranslations = {en:{}, ru:{}};

TranslationsArray.forEach( translationObject => {
    config.supportedLangs.forEach( lang => {
        ConnectedTranslations[lang] = {
            ...ConnectedTranslations[lang],
            ...translationObject[lang]
        };
    })
});

console.log('start:', TranslationsArray, 'finish:', ConnectedTranslations );

export default ConnectedTranslations;
