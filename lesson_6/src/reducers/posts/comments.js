import { GET_DATA_REQUESTED, GET_POST_COMMENTS_SUCCESS, POST_ERROR } from '../../constants';

const postsInitialState = {
    loading: false,
    loaded: false,
    list: [],
    errors: []
};

const comments = ( state = postsInitialState, action) => {
    switch( action.type ){

        case GET_DATA_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case POST_ERROR:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.data
            };

        case GET_POST_COMMENTS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                list: action.data
            };

        default:
            return state;
    }
};

export default comments;
