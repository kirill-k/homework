import { GET_DATA_REQUESTED, GET_USER_POSTS_SUCCESS, POST_ERROR } from '../../constants';

const postsInitialState = {
    loading: false,
    loaded: false,
    posts: [],
    errors: []
};

const userPosts = ( state = postsInitialState, action) => {
    switch( action.type ){

        case GET_DATA_REQUESTED:
            return {
                ...state,
                loading: true
            };

        case POST_ERROR:
            return {
                ...state,
                loading: false,
                loaded: false,
                error: action.data
            };

        case GET_USER_POSTS_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                posts: action.data
            };

        default:
            return state;
    }
};

export default userPosts;
