/* eslint-disable */
import React from 'react';
import MainComponent from './index'
import PostBody from './postBody'
import AuthorPosts from './authorPosts'
import PostComments from './comments'

const notFound = () => (<div className="mainLayout">404</div>)

export const Routes = [
    {
        path: '/',
        exact: true,
        component: MainComponent,
    },
    {
        path: '/posts/:id',
        exact: true,
        component: PostBody
    },
    {
        path: '/posts/:id/comments',
        component: PostComments
    },
    {
        path: '/user/:id',
        component: AuthorPosts
    },
    {
        exact: true,
        component: notFound
    }
];