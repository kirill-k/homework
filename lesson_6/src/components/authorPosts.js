/* eslint-disable */
import React, { Component } from 'react'
import Loading from './common/loader'
import { connect } from 'react-redux';
import * as postActions from '../actions/userPosts';

import '../index.css'

class AuthorPosts extends Component{

    componentDidMount(){
        const id = this.props.match.params.id;
        this.props.getUserPostsData(id);
    }

    render = () => {
        const {
            userPosts: {
                posts,
                loading
            }
        } = this.props;

        if( loading === true){
            return <Loading />
        } else {
            return (
                <div className="postBlock">
                    {
                        posts.map(post => {
                            return (
                                <div key={post.id}>
                                    <p>{post.body}</p>
                                    <h2>{post.title}</h2>
                                </div>
                            )
                        })
                    }
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        userPosts: state.userPosts
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserPostsData: data => dispatch(postActions.getUserPostsData(data)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(AuthorPosts);
