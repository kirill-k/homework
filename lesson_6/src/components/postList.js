/* eslint-disable */
import React, { Component } from 'react'
import PostItem from './postItem'

import '../index.css'

class PostList extends Component{
    state = {
        loading: true,
        data: [],
        dataShow: []
    };

    componentWillReceiveProps = (nextProps) => {

        if (this.props.posts != nextProps.posts) {
            const { dataShow } = this.state;

            this.setState({
                data: nextProps.posts.slice(0, nextProps.posts.length - 50),
                dataShow: [...nextProps.posts.slice(-50), ...dataShow],
            })
        }
        return false;
    };

    showMorePosts = () => {
        const { data, dataShow } = this.state;

        this.setState({
            data: data.slice(0, data.length - 25),
            dataShow: [...data.slice(-25), ...dataShow]
        });
    };


    render() {

        return (
            <div className="posts_container">
                {
                    this.state.dataShow.map((postItem, index) => {
                        return <PostItem data={postItem} key={`${postItem.id}-${index}`}>{postItem.title}</PostItem>
                    })
                }
                <div className="buttonBlock">
                    { this.state.data.length !== 0 ? <button onClick={this.showMorePosts}>Show more</button> : null }
                </div>
            </div>
        );
    }
}


export default PostList;
