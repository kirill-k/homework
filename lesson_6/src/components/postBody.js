/* eslint-disable */
import React, { Component } from 'react'
import Loading from './common/loader'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import * as postActions from '../actions/postItem';

import '../index.css'

class PostBody extends Component{

    componentDidMount(){
        const id = this.props.match.params.id;
        this.props.getPostItemData(id)
    }

    render () {
        const { postItem, match } = this.props;

        if( postItem.loading === true){
            return(<Loading />)
        } else {
            return(
                <div className="postBlock">
                    <Link to={`/user/${postItem.post.userId}`}>{`Author: #${postItem.post.userId}`}</Link>
                    <h3>{postItem.post.title}</h3>
                    <p>{postItem.post.body}</p>
                    <Link to={`${match.url}/comments/`}>Comments</Link>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return { postItem: state.postItem };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getPostItemData: data => dispatch(postActions.getPostItemData(data)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(PostBody);
