/* eslint-disable */
import React, { Component } from 'react'
import Loading from './common/loader'
import { connect } from 'react-redux';
import * as postActions from '../actions/postComments';
import '../index.css'

class PostComments extends Component{

    componentDidMount(){
        const id = this.props.match.params.id;

        this.props.getPostCommentsData(id)
    }

    renderPostCommentsById = () => {
        const { comments } = this.props;
        const id = this.props.match.params.id;

        const commentsById = comments.list.filter((item) => {
            return id == item.postId;
        });
        return commentsById.map((item) => {
            return (
                <div key={item.id}>
                    <h3>{`Author: ${item.email}`}</h3>
                    <h4>{`Post: ${item.name}`}</h4>
                    <p>{item.body}</p>
                </div>
            )
        })
    };


    render() {
        const { comments: { loading } } = this.props;
        const id = this.props.match.params.id;
        if (loading === true) {
            return <Loading />
        } else {
            return (
                <div className="postBlock">
                    <h1>{`Comments Author #${id}:`}</h1>
                    {this.renderPostCommentsById()}
                </div>
            );
        }
    }
}


const mapStateToProps = (state) => {
    return { comments: state.comments };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getPostCommentsData: data => dispatch(postActions.getPostCommentsData(data)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(PostComments);
