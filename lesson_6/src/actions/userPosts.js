/* eslint-disable */
import React from 'react'
import * as actionTypes from '../constants';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const getUsersDataDone = data => {
    return {
        type: actionTypes.GET_USER_POSTS_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.POST_ERROR
    };
};

export const getUserPostsData = (id) => dispatch => {
    const url = `https://jsonplaceholder.typicode.com/posts?userId=${id}`;

    dispatch(getDataRequested());

    fetch(url)
        .then(response => response.json())
        .then(data => {
            dispatch(getUsersDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
}