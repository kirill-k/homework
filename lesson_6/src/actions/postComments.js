/* eslint-disable */
import React from 'react'
import * as actionTypes from '../constants';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const getDataCommentsDone = data => {
    return {
        type: actionTypes.GET_POST_COMMENTS_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.POST_ERROR
    };
};

export const getPostCommentsData = (id) => dispatch => {
    const url = `https://jsonplaceholder.typicode.com/posts/${id}/comments`;

    dispatch(getDataRequested());
    fetch(url)
        .then(response => response.json())
        .then(data => {
            dispatch(getDataCommentsDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
}