/* eslint-disable */
import React from 'react'
import * as actionTypes from '../constants';


const getDataRequested = () => {
    return {
        type: actionTypes.GET_DATA_REQUESTED
    };
};

const getDataDone = data => {
    return {
        type: actionTypes.POST_SUCCESS,
        data: data
    };
};

const getDataFailed = () => {
    return {
        type: actionTypes.POST_ERROR
    };
};

export const getPostsData = () => dispatch => {
    const url = 'https://jsonplaceholder.typicode.com/posts';

    dispatch(getDataRequested());

    fetch(url)
        .then(response => response.json())
        .then(data => {
            dispatch(getDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
}

export const getMorePostsData = () => dispatch => {
    const url = 'https://jsonplaceholder.typicode.com/posts';

    dispatch(getDataRequested());

    fetch(url)
        .then(response => response.json())
        .then(data => {
            dispatch(getDataDone(data));
        })
        .catch(error => {
            dispatch(getDataFailed(error));
        })
}