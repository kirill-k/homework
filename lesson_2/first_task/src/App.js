import React, { Component } from 'react'
import './App.css';
import LoaderImg from './components/loaderImg'

class App extends Component {

  render() {
    return (
      <div className="App">
         <LoaderImg url='https://pngimg.com/uploads/mars_planet/mars_planet_PNG15.png' />
      </div>
    );
  }
}

export default App;
