/* eslint-disable */
import React, { Component } from 'react'
import Loading from './loader'

class LoaderImg extends Component {
    state = {
        loader: true
    };

    componentDidMount = () => {
        const {url} = this.props;
        let image = new Image();

        image.src = url;
        image.onload = () => {this.setState({ loader: false })}
    };

    render() {
        const {loader} = this.state;
        const {url} = this.props;

        return (
                <div>
                    {
                        loader?
                            <Loading color="green" size="80" thickness="20" duration="2" /> :
                            <img className="planet" src={url} />
                    }
                </div>

        )
    }
}

export default LoaderImg;