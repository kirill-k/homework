/* eslint-disable */
import React, {Component} from 'react'

const loaderStyles = {
    color: '#fff',
    animation: '1.5s linear infinite gotofritz-spin',
    backfaceVisibility: 'hidden',
    borderColor: '#fff',
    borderRadius: '50%',
    borderStyle: 'solid',
    borderWidth: '8px',
    borderLeftColor: 'transparent',
    display: 'inline-block',
    height: '50px',
    width: '50px'
};

const titleStyles = {
    color: '#fff',
    marginBottom: '25px'
};

const Loading = () => {

    const STYLE_ID = 'gotofritz_easy';
    if (!document.getElementById(STYLE_ID)) {
        const styleEl = document.createElement('style');
        styleEl.id = STYLE_ID;
        styleEl.textContent = '@keyframes gotofritz-spin { to { transform: rotate(360deg); } }';
        document.getElementsByTagName('head')[0].appendChild(styleEl);
    }
    return(
        <div>
            <div style={titleStyles}>
                Please wait ...
            </div>
            <div style={loaderStyles} />
        </div>
        )
};

export default Loading;
