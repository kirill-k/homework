import React, { Component } from 'react'
import MainTableComponent from './Table/index'
import '../App.css'

class CustomTable extends Component {

    render(){
        return(
            <div className="viewContainer">
                <h1 className="title">Table</h1>
                <MainTableComponent />
            </div>
        )
    }
}

export default CustomTable;