/* eslint-disable */
import React, { Component } from 'react'

const CellT = props => {
    const defineStyle = props.cellStyle;
    const style = {
        textAlign: 'left',
        fontStyle: 'normal',
        width: defineStyle.width,
        color: defineStyle.color,
        background: defineStyle.background
    };

    return <td style={style}>{props.children}</td>
};

export default CellT;