/* eslint-disable */
import React, { Component } from 'react'

const CellD = props => {
    const defineStyle = props.cellStyle;
    const style = {
        textAlign: 'left',
        fontStyle: 'italic',
        width: defineStyle.width,
        color: defineStyle.color,
        background: defineStyle.background
    };

    return <td style={style}>{props.children}</td>
};

export default CellD;