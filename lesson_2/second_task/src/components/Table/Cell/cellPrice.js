/* eslint-disable */
import React, { Component } from 'react'

const CellP = props => {
    const defineStyle = props.cellStyle;
    const style = {
        textAlign: 'right',
        fontStyle: 'normal',
        width: defineStyle.width,
        color: defineStyle.color,
        background: defineStyle.background
    };

    return <td style={style}>{props.children}</td>
};

export default CellP;