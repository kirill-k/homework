/* eslint-disable */
import React, { Component } from 'react'
import CellD from './cellDate'
import CellT from './cellText'
import CellP from './cellPrice'
import CellN from './cellNumber'

const Cell = props => {
    const width = props.cells;
    const type = props.type.toLowerCase();
    const currency = props.currency;
    const color = props.color;
    const bg = props.background;
    const children = props.children;

    let countWidth = width * 100;
    const columnStyle = {
        width: `${countWidth}px`,
        color: color,
        background: bg
    };

    const setCurrency = currency == undefined ? '' : currency;

    const renderColumnWithType = () => {
        if (type ==='number'){
            return <CellN cellStyle={columnStyle}>{children}</CellN>
        } else if (type ==='text') {
            return <CellT cellStyle={columnStyle}>{children}</CellT>
        } else if (type ==='money') {
            return <CellP cellStyle={columnStyle}>{
                type==='money' && !currency ?
                    console.log(`Please add currency where text is '${children}'`) :
                    null
            }{`${children} ${setCurrency}`}</CellP>
        } else if (type ==='date') {
            return <CellD cellStyle={columnStyle}>{children}</CellD>
        } else {
            return children
        }
    };

    return renderColumnWithType();
};

Cell.defaultProps = {
    type: 'TEXT',
    cells: 1,
    background: 'transparent',
    color: '#666000'
};

export default Cell;