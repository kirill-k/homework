/* eslint-disable */
import React, { Component } from 'react'

const Row = ({children, head}) => {

    return(
            head ?
                <thead>
                    <tr>{children}</tr>
                </thead> :
                <tbody>
                    <tr>{children}</tr>
                </tbody>
    )
};

export default Row;

Row.defaultProps = {
    head: false
};