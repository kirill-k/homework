/* eslint-disable */
import React, { Component } from 'react'
import '../../../App.css'


class Table extends Component {

    render(){
        const {children} = this.props;
        return <table>{children}</table>
    }
}

export default Table;