import React, { Component } from 'react'
import '../../App.css'
import Table from './Table/index'
import Row from './Row/index'
import Cell from '../Table/Cell/index'

class MainTableComponent extends Component {

    render(){
        return (
            <Table>
                <Row head={true}>
                    <Cell type="date">text1</Cell>
                    <Cell type="text" color="red" background="#000">text2</Cell>
                    <Cell type="number">text3</Cell>
                    <Cell type="money">Price</Cell>
                </Row>
                <Row>
                    <Cell type="date" background="yellow">text1</Cell>
                    <Cell type="text" cells={3}>text2</Cell>
                    <Cell type="number" cells={1}>text3</Cell>
                    <Cell type="money" cells={2} currency="₴">150,000</Cell>
                </Row>
                <Row>
                    <Cell type="date" color="black">text1</Cell>
                    <Cell type="text" cells={3}>text2</Cell>
                    <Cell type="number" cells={3}>text3</Cell>
                    <Cell type="money" cells={2} currency="$">2,000</Cell>
                </Row>
            </Table>
        )
    }
}

export default MainTableComponent;