import React, { Component } from 'react'
import CustomTable from './components/index'
import './App.css'

class App extends Component {
  
  render() {
    return (
      <div className="App">
        <CustomTable />
      </div>
    );
  }
}

export default App;
