/* eslint-disable */
import React, {Component} from 'react'
import PropTypes from 'prop-types';
import './button.css'

class CustomButton extends Component {

    render(){
        const {action, type, value} = this.props;

        return(
                <input onClick={action}
                       type={type}
                       value={value}
                />
        )
    }
}

CustomButton.propTypes = {
    action: PropTypes.func,
    type: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired
};

export default CustomButton;