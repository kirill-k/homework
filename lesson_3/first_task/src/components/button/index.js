/* eslint-disable */
import React, {Component} from 'react'
import ReactDOM from 'react-dom';
import CustomButton from './customButton';
import './button.css'

class Button extends Component {

    focusTextInput = (e) => {
        const customBtn1 = ReactDOM.findDOMNode(this._customBtn1);
        const customBtn2 = ReactDOM.findDOMNode(this._customBtn2);
        const customBtn3 = ReactDOM.findDOMNode(this._customBtn3);
        const customBtn4 = ReactDOM.findDOMNode(this._customBtn4);

        switch (e.target){
            case (customBtn1):
                console.log(customBtn1);
                customBtn1.className="blam";
                break;
            case (customBtn2):
                console.log(customBtn2);
                customBtn2.className="red";
                break;
            case (customBtn3):
                console.log(customBtn3);
                customBtn3.className="later";
                break;
            case (customBtn4):
                console.log(customBtn4);
                customBtn4.className="red";
                break;
            default:
                return;
        }
    };

    render(){

        return(
            <div className='group'>
                <CustomButton action={this.focusTextInput}
                       type='button'
                       ref={(node) => { this._customBtn1 = node; }}
                       value='button 1'
                />
                <CustomButton action={this.focusTextInput}
                       type='button'
                       ref={(node) => { this._customBtn2 = node; }}
                       value='button 2'
                />
                <CustomButton action={this.focusTextInput}
                       type='button'
                       ref={(node) => { this._customBtn3 = node; }}
                       value='button 3'
                />
                <CustomButton action={this.focusTextInput}
                       type='button'
                       ref={(node) => { this._customBtn4 = node; }}
                       value='button 4'
                />
            </div>
        )
    }
}

export default Button;