import React, { Component } from 'react';
import MainComponent from './components/index'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <MainComponent />
      </div>
    );
  }
}

export default App;
