/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './index.css'

class UploadButton extends Component {


    render() {
        const {
            action,
            id,
            name,
            type,
            style
        } = this.props;

        return (
            <input type={type}
                   id={id}
                   className={style}
                   name={name}
                   onChange={action}
            />
        )
    }
}

UploadButton.propTypes = {
    action: PropTypes.func,
    type: PropTypes.string.isRequired,
    name: PropTypes.string,
};


export default UploadButton;