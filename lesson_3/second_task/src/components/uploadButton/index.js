/* eslint-disable */
import React, { Component } from 'react';
import UploadButton from './uploadButton'
import Loader from '../../utils/loader/index'
import './index.css'

class UploadComponent extends Component {

    state = {
        image: 'http://ajaxuploader.com/images/drag-drop-file-upload.png',
        loader: true
    };

    componentDidMount = () => {
        let image = new Image();
        image.src = this.state.image;

        image.onload = () => {this.setState({ loader: false })}
    };

    onImageChange = (event) => {
        this.setState({loader: true});
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({image: e.target.result, loader: false});
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    };

    render() {
        const {loader, image} = this.state;

        return (
            <div className="image-upload">
                {
                    loader ?
                        <Loader color="green" size="80" thickness="20" duration="2"/>
                        :
                        <>
                            <label htmlFor="file">
                                <img className='uploadImg' src={image} />
                            </label>
                            <UploadButton type="file"
                                          id="file"
                                          style="uploadBtn"
                                          name="file"
                                          action={this.onImageChange}
                            />
                        </>
                }
            </div>
        )
    }
}

export default UploadComponent;