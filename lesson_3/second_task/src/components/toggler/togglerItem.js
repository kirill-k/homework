/* eslint-disable */
import React, {Component} from 'react'
import PropTypes from 'prop-types';
import './button.css'

export const TogglerItem = props => {
    const {name, active, action, style} = props;

    return(
        <div className={
            active === true ?
                `${style} active`:
                style
        }
             data-value={name}
             onClick={typeof action === "function" ? action : null}

        >
            {name}
        </div>
    );
};

TogglerItem.propTypes = {
    name: PropTypes.string.isRequired,
};

export default TogglerItem;