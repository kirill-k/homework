/* eslint-disable */
import React, {Component} from 'react'
import Toggler from './toggler';
import TogglerItem from './togglerItem';
import './button.css'

class Button extends Component {

    state = {active: 'left'};

    handleChangeStatus = (e) => {
        let value = e.target.dataset.value;
        this.setState({active: value});
    };

    render(){
        const {active} = this.state;

        return(
            <Toggler
                name="Choose item"
                active={active}
                changeStatus={this.handleChangeStatus}
            >
                <TogglerItem style="blam" name="left" />
                <TogglerItem style="blam" name="center" />
                <TogglerItem style="blam" name="right" />
            </Toggler>
        )
    }
}

export default Button;