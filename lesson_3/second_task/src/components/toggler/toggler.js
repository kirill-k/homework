/* eslint-disable */
import React, {Component} from 'react'
import './button.css'

class Toggler extends Component {

    render(){
        const {children, name, active, changeStatus} = this.props;

        return(
            <div>
                <h1>{name}</h1>
                <h2>{`You chose ${active}`}</h2>
                <div className='group'>
                    {
                        React.Children.map(children, (item) => {
                                if(item.props.name === active){
                                    return React.cloneElement(item, {
                                        name: item.props.name,
                                        active: true,
                                        action: item
                                    })
                                } else {
                                    return React.cloneElement(item, {
                                        name: item.props.name,
                                        action: changeStatus
                                    })
                                }
                            }
                        )
                    }
                </div>
            </div>
        )
    }
}

export default Toggler;