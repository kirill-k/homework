/* eslint-disable */
import React, { Component } from 'react';
import Button from './toggler/index'
import UploadComponent from './uploadButton/index'
import Input from './input/index'

class MainComponent extends Component {
    render() {
        return (
            <div>
                <Button />
                <UploadComponent />
                <Input />
            </div>
        );
    }
}

export default MainComponent;