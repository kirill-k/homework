/* eslint-disable */
import React, {Component} from 'react'
import PropTypes from 'prop-types';

class CustomInputName extends Component {
    state = {
        value: '',
        error: ''
    };

    handleOnChange = (e) => {

        this.setState({value: e.target.value},
            () => this.handleValidation());
    };

    handleValidation = () => {
        const {value} = this.state;
        var hasNumber = /\d/;

        if (this.state.value.length == 0){
            this.setState({error: 'Name should not be empty'})
        } else if (value.length >= 20){
            this.setState({error: 'Name should be less then 20 character'})
        } else if (hasNumber.test(value)){
            this.setState({error: 'Name should not have digits'})
        } else {
            this.setState({error: ''})
        }
    };

    render() {
        const {placeholder, type} = this.props;

        return (
            <>
                <input onChange={(e)=>this.handleOnChange(e)}
                       type={type}
                       name="name"
                       placeholder={placeholder}
                />
                <label className="error" htmlFor="name">
                    {this.state.error}
                </label>
            </>
        )
    }
}

CustomInputName.propTypes = {
    placeholder: PropTypes.string,
    type: PropTypes.string
};

export default CustomInputName;