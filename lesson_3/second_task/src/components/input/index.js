/* eslint-disable */
import React, { Component } from 'react';
import CustomInputName from './inputName'
import CustomInputPassword from './inputPassword'
import './index.css'

class Input extends Component {

    render() {

        return (
            <form className="inputBlock">
                <div className="inputBlock">
                    <label>Name:</label>
                    <CustomInputName type="text" placeholder='Name' />
                </div>
                <div className="inputBlock">
                    <label>Password:</label>
                    <CustomInputPassword type="password" placeholder='Password' />
                </div>
                <button className="loginBtn"
                        onSubmit={()=>console.log('submit')}
                >
                    Login
                </button>
            </form>
        )
    }
}

export default Input;