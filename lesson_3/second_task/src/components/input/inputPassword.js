/* eslint-disable */
import React, {Component} from 'react'
import PropTypes from 'prop-types';

class CustomInputPassword extends Component {

    state = {
        value: '',
        error: ''
    };

    handleOnChange = (e) => {

        this.setState({value: e.target.value},
            () => this.handleValidation());
    };

    handleValidation = () => {
        const {value} = this.state;

        if (this.state.value.length == 0){
            this.setState({error: 'Password not be empty'})
        } else if (value.length <= 8){
            this.setState({error: 'Password should be more then 8 character'})
        } else {
            this.setState({error: ''})
        }
    };

    render() {
        const {error} = this.state;
        const {placeholder, type} = this.props;

        return (
            <>
                <input onChange={(e)=>this.handleOnChange(e)}
                   type={type}
                   name="password"
                   placeholder={placeholder}
                />
                <label className="error"
                   htmlFor="password"
                >
                    {error}
                </label>
            </>
        )
    }
}

CustomInputPassword.propTypes = {
    placeholder: PropTypes.string,
    type: PropTypes.string
};

export default CustomInputPassword;