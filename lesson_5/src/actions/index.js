import * as actionTypes from '../constants';

export const createTask = (task) => {
    return {
        type: actionTypes.ADD_TASK, task: task }
};

export const toggleTask = (id) => {
    return { type: actionTypes.TOGGLE_TASK, id: id }
};

export const deleteTask = (id) => {
    return { type: actionTypes.REMOVE_TASK, id: id }
};

export const filterCompletedTask = () => {
    return { type: actionTypes.COMPLETED_TASKS }
};

export const getWaitingTasks = () => {
    return { type: actionTypes.WAITING_TASKS }
};

export const getAllTasks = () => {
    return { type: actionTypes.ALL_TASKS }
};