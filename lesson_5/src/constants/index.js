export const ADD_TASK = 'ADD_TASK';
export const TOGGLE_TASK = 'TOGGLE_TASK';
export const REMOVE_TASK = 'REMOVE_TASK';
export const COMPLETED_TASKS = 'COMPLETED_TASKS';
export const WAITING_TASKS = 'WAITING_TASKS';
export const ALL_TASKS = 'ALL_TASKS';
