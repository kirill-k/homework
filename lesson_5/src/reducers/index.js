/* eslint-disable */
import {
    ADD_TASK,
    TOGGLE_TASK,
    REMOVE_TASK,
    COMPLETED_TASKS,
    WAITING_TASKS,
    ALL_TASKS
} from '../constants';

const initialState = {
    list: [],
    filtered: []
};

function reducer(state = initialState, action){
    switch( action.type ){

        case ADD_TASK:
            return {
                ...state,
                list: [
                    ...state.list,
                    {
                        id: state.list.length,
                        name: action.task,
                        done: false
                    }
                ]
            };

        case TOGGLE_TASK:
            return {...state, list:[...state.list].map(todo =>
                todo.id === action.id ? { ...todo, done: !todo.done } : todo
            )};

        case REMOVE_TASK:
            return {...state, list:[...state.list].filter((item)=>{
                return item.id != action.id
                })
            };

        case COMPLETED_TASKS:
            return {...state, filtered: state.list.filter(task =>
                task.done
            )};

        case WAITING_TASKS:
            return {...state, filtered: state.list.filter(task =>
                !task.done
            )};

        case ALL_TASKS:
            return {...state, filtered: state.list};
        default:
            return state;
    }
}



export default reducer;
