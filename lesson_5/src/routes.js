/* eslint-disable */
import React from 'react';
import MainComponent from './components'
import TaskList from './components/list'

const notFound = () => (<div className="mainLayout">Not Found 404</div>)

export const Routes = [
    {
        path: '/',
        exact: true,
        component: MainComponent,
    },
    {
        path: '/todo/:status',
        exact: true,
        component: TaskList,
    },
    {
        exact: true,
        component: notFound
    }
];