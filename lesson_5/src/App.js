import React, { Component } from 'react';
import {Provider} from 'react-redux'
import store from './redux/store';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {Routes} from './routes'
import TopMenu from './components/topMenu'
import './App.css';

class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <div>
                    <TopMenu />
                    <Switch>
                        {
                            Routes.map((route, index) => {
                                return <Route key={index} {...route} />
                            })
                        }
                    </Switch>
                </div>
            </BrowserRouter>
        </Provider>
    );
  }
}

export default App;
