/* eslint-disable */
import React, { Component } from 'react'
import { connect } from 'react-redux';

class TaskList extends Component {

    render() {

        return (
            <div>
                <h1>{`${this.props.match.params.status} tasks`}</h1>
                <ul>
                    {this.props.data.filtered.length>0?
                        this.props.data.filtered.map((item) => {
                            return <li key={item.id}>{item.name}</li>
                        }):
                        null
                    }
                </ul>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    data: state,
});

export default connect(mapStateToProps)(TaskList);