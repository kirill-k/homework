/* eslint-disable */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import * as taskAction from '../actions';
import './index.css'


class MainComponent extends Component {
    state = { taskValue: '' };

    handleChange = (e) => {
        const value = e.target.value;
        this.setState({taskValue: value})
    };

    handleSubmit = (e) => {
        e.preventDefault();
        let name= this.state.taskValue;

        this.setState({ taskValue: '' });
        this.props.createTask(name);
    };

    handleToggleTask = (id) => {
        this.props.setCompletedTask(id);
    };

    handleDeleteTask = (id) => {
        this.props.removeTask(id);
    };

    getCompletedTasks = () => {
        this.props.getCompletedTasks();
    };

    getWaitingTasks = () => {
        this.props.getWaitingTasks();
    };

    getAllTasks = () => {
        this.props.getAllTasks();
    };


    render() {
        const { data, data: filtered } = this.props;
        const {taskValue} = this.state;
        const renderData = filtered.length === 0 ? data : filtered

        return (
            <div className="appBody">
                <div className="menu">
                    <NavLink onClick={this.getAllTasks} className="filterItem" to="/todo/all"> ALL </NavLink>
                    <NavLink onClick={this.getCompletedTasks} className="filterItem" to="/todo/completed"> Completed </NavLink>
                    <NavLink onClick={this.getWaitingTasks} className="filterItem" to="/todo/waiting"> Waiting </NavLink>
                </div>
                <h1>{renderData.list.length > 0?
                    (renderData.list.length > 1?
                        `There is ${data.list.length} tasks` :
                        `There is ${data.list.length} task`
                    ) : `Task List`}</h1>
                <div className="addForm">
                    <label>Add task</label>
                    <input
                        onChange={this.handleChange}
                        value={taskValue}
                        type="text"
                        placeholder="add to do list"
                    />
                    <button onClick={this.handleSubmit}>add</button>
                </div>
                <ul>
                    {
                        typeof data.list !== 'undefined'?
                            data.list.map((item)=> {
                            return (
                                <span
                                    key={item.id}
                                    className={`taskItem ${item.done?'done':'waiting'}`}
                                >
                                    <li>{item.name}</li>
                                    <input onChange={() => this.handleToggleTask(item.id)} type="checkbox" />
                                    <button onClick={() => this.handleDeleteTask(item.id)}>Remove</button>
                                </span>

                            )
                        }): null
                    }
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    data: state,
});

const mapDispatchToProps = (dispatch) => {
    return {
        createTask: task => dispatch(taskAction.createTask(task)),
        setCompletedTask: id => dispatch(taskAction.toggleTask(id)),
        removeTask: id => dispatch(taskAction.deleteTask(id)),
        getCompletedTasks: () => dispatch(taskAction.filterCompletedTask()),
        getWaitingTasks: () => dispatch(taskAction.getWaitingTasks()),
        getAllTasks: () => dispatch(taskAction.getAllTasks()),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(MainComponent);
