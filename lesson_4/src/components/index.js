/* eslint-disable */
import React from 'react'
import { BrowserRouter, Route, Switch, Link, NavLink, Prompt } from 'react-router-dom';
import HeaderMenu from './menu'
import PrivateRoute from './privateRoute'
import {Routes} from './routes'
import '../App.css'

const Secret = () => (<div>Secret</div>);
const Root = () => {

    const limitPosts = 25;

    return (
        <div>
            <BrowserRouter>
                <div>
                    <div className="menu">
                        <HeaderMenu limit={limitPosts} />
                    </div>
                    <Switch>
                        <PrivateRoute
                            path="/secret"
                            isAuthenticated={false}
                            component={Secret}
                        />
                        {
                            Routes.map((route, index) => {
                                return <Route key={index} {...route} />
                            })
                        }
                    </Switch>
                </div>
            </BrowserRouter>
        </div>
    )
};

export default Root;