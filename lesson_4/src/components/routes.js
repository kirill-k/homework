/* eslint-disable */
import React from 'react';
import MainComponent from './posts'
import PostBody from './posts/postBody'
import PostsLimit from './posts/limit'
import PostComments from './posts/comments'

const notFound = () => (<div className="mainLayout">404</div>)

export const Routes = [
    {
        path: '/',
        exact: true,
        component: MainComponent,
    },
    {
        path: '/posts/:id/comments',
        component: PostComments
    },
    {
        path: '/posts/limit/:limit',
        component: PostsLimit
    },
    {
      path: '/posts/:id',
      component: PostBody
    },
    {
        path: '/posts/limit/:items',
        component: PostComments
    },
    {
        exact: true,
        component: notFound
    }
];