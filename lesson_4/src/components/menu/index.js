/* eslint-disable */
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';

class HeaderMenu extends Component {

    render() {
        const {limit} = this.props;
        return (
            <>
                <NavLink exact to="/">Main </NavLink>
                <NavLink to={`/posts/limit/${limit}`}>Posts</NavLink>
            </>
        )
    }
}

export default HeaderMenu;