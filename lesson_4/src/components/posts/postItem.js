/* eslint-disable */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../index.css'

class PostItem extends Component {
    render() {
        const postItem = this.props.data;

        return <Link to={`/posts/${postItem.id}`}>{postItem.title}</Link>
    }
}

export default PostItem;