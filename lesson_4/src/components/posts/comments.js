/* eslint-disable */
import React, { Component } from 'react'
import Loading from '../common/loader'
import '../index.css'

class PostComments extends Component{
    state = {
        loading: true,
        comments: []
    };

    componentDidMount(){

        this.getCurrentPostItem()
    }

    getCurrentPostItem = () => {
        let { match } = this.props;
        const { comments } = this.state;

        fetch(`https://jsonplaceholder.typicode.com/posts/${match.params.id}/comments`)
            .then(response => response.json())
            .then((data) => {
                const result = data.filter((item) => {
                    return match.params.id == item.postId;
                });

                this.setState({
                    comments: result,
                    loading: false
                })
            });
    };

    render = () => {
        let { comments, loading } = this.state;
        if( loading === true){
            return(<Loading />)
        } else {
            return(
                <div className="postBlock">
                    <h1>Comments:</h1>
                    {
                        comments.map((item) => {
                            return (
                                <div key={item.id}>
                                    <h3>{`Author: ${item.email}`}</h3>
                                    <h4>{`Post: ${item.name}`}</h4>
                                    <p>{item.body}</p>
                                </div>
                            )
                    })
                    }
                </div>
            );
        }
    }
}

export default PostComments;
