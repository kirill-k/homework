/* eslint-disable */
import React, { Component } from 'react'
import PostItem from './postItem'
import Loading from '../common/loader'
import '../index.css'

class MainComponent extends Component {
    state = {
        data: [],
        dataShow: [],
        loading: true,
    };

    componentDidMount = () => {
        this.getPostsData();
    };
    
    getPostsData = () => {
        const { dataShow } = this.state;

        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(data => this.setState({
                data: data.slice(0, data.length - 20),
                dataShow: [...data.slice(-20), ...dataShow],
                loading: false
            }));
    };

    showMorePosts = () => {
        const { data, dataShow } = this.state;

        this.setState({
            data: data.slice(0, data.length - 20),
            dataShow: [...data.slice(-20), ...dataShow]
        });

    };

    render() {
        const { data, dataShow, loading } = this.state;

        return (
            <div className="wrapper">
                {
                    loading ? <Loading /> : <div className="posts_container">
                        {
                            dataShow.map((postItem) => {
                                return <PostItem data={postItem} key={postItem.id}>{postItem.title}</PostItem>
                            })
                        }
                    </div>
                }
                <div className="buttonBlock">
                    { data.length !== 0 ? <button onClick={this.showMorePosts}>Show more</button> : null }
                </div>
            </div>
        )
    }
}

export default MainComponent;