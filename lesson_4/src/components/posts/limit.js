/* eslint-disable */
import React, { Component } from 'react'
import PostItem from './postItem'
import Loading from '../common/loader'
import '../index.css'

class PostsLimit extends Component {
    state = {
        data: [],
        loading: true,
    };

    componentDidMount = () => {
        this.getPostsData();
    };

    getPostsData = () => {
        let limit = this.props.match.params.limit;

        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(data => this.setState({
                data: [...data.slice(-limit)],
                loading: false
            }));
    };

    render() {
        const { data, loading } = this.state;

        return (
            <div className="wrapper">
                <h1>{`Last ${data.length} Posts`}</h1>
                {
                    loading ? <Loading /> : <div className="posts_container">
                        {
                            data.map((postItem) => {
                                return <PostItem data={postItem} key={postItem.id}>{postItem.title}</PostItem>
                            })
                        }
                    </div>
                }
            </div>
        )
    }
}

export default PostsLimit;