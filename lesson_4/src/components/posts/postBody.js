/* eslint-disable */
import React, { Component } from 'react'
import Loading from '../common/loader'
import { Link } from 'react-router-dom'
import '../index.css'

class PostBody extends Component{
    state = {
        loading: true,
        post: {},
    };

    componentDidMount(){
        this.getCurrentPostItem()
    }

    getCurrentPostItem = () => {

        let { match } = this.props;
        fetch(`https://jsonplaceholder.typicode.com/posts/${match.params.id}`)
            .then(response => response.json())
            .then(json => {
                this.setState({
                    post: json,
                    loading: false
                })
            })
    };

    render = () => {
        const { match } = this.props;
        let { post, loading } = this.state;

        if( loading === true){
            return(<Loading />)
        } else {
            return(
                <div className="postBlock">
                    <h2>{post.title}</h2>
                    <p>{post.body}</p>
                    <Link to={`/posts/${match.params.id}/comments`}>Show comments</Link>
                </div>
            );
        }
    }
}

export default PostBody;
